import pytest

ENDPOINT = '/api/contracts/'

from utils.subgroup_util import Subgroup

@pytest.mark.django_db
class TestContractEndpoint:
    def setup_method(self):
        self.contract_test_supply_voltage_1 = 24
        self.contract_test_supply_voltage_2 = 30
        self.contract_test_supply_voltage_3 = 43
        self.contract_test_supply_voltage_4 = 69
        self.contract_test_supply_voltage_5 = 88
        self.contract_test_supply_voltage_6 = 137
        self.contract_test_supply_voltage_7 = 230
        self.contract_test_supply_voltage_8 = -4


    def test_get_what_subgroup_contract_is_A4(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_1) == Subgroup.A4

    def test_get_what_subgroup_contract_is_A3A(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_2) == Subgroup.A3A

    def test_get_what_subgroup_contract_is_A3a(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_3) == Subgroup.A3A

    def test_get_what_subgroup_contract_is_A3(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_4) == Subgroup.A3

    def test_get_what_subgroup_contract_is_A2(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_5) == Subgroup.A2

    def test_get_what_subgroup_contract_is_a2(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_6) == Subgroup.A2

    def test_get_what_subgroup_contract_is_A1(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_7) == Subgroup.A1

    def test_throws_exception_when_suply_voltage_does_not_match_ranges(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.contract_test_supply_voltage_8)

        assert 'Subgroup not found' in str(e.value)
