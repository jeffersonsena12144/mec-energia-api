from datetime import datetime
from utils.date_util_analyze import DateUtilsAnalyze

def test_analyze_date_is_weekend():
    assert DateUtilsAnalyze(datetime(2023, 12, 2)).analyze_date().is_weekend()

def test_analyze_date_is_weekday():
    assert DateUtilsAnalyze(datetime(2023, 12, 4)).analyze_date().is_weekday()

def test_analyze_date_days_between_dates():
    date1 = datetime(2023, 12, 1)
    date2 = datetime(2023, 12, 5)
    assert DateUtilsAnalyze(date1).analyze_date().days_between_dates(date2) == 4

def test_analyze_date_format_date():
    date = datetime(2023, 12, 4)
    assert DateUtilsAnalyze(date).analyze_date().format_date("%Y/%m/%d") == "2023/12/04"
