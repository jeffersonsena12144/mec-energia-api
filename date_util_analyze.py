import datetime

class DateUtilsAnalyze:
    def __init__(self, date):
        self.date = date

    def analyze_date(self):
        return self

    def is_weekend(self):
        return self.date.weekday() in [5, 6]
        
    def is_weekday(self):
        return not self.is_weekend()
    
    def days_between_dates(self, other_date):
        delta = abs(other_date )
        return delta.days

    def format_date(self, format_str="%Y/%m/%d"):
        return self.date.strftime(format_str)
